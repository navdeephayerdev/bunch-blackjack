﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 Players are each dealt two cards, face up or down depending on the casino and the table. In the U.S., the dealer is also dealt two cards, normally one up (exposed) and one down (hidden). In most other countries, the dealer only receives one card face up. The value of cards two through ten is their pip value (2 through 10). Face cards (Jack, Queen, and King) are all worth ten. Aces can be worth one or eleven. A hand's value is the sum of the card values. Players are allowed to draw additional cards to improve their hands. A hand with an ace valued as 11 is called "soft", meaning that the hand will not bust by taking an additional card. The value of the ace will become one to prevent the hand from exceeding 21. Otherwise, the hand is called "hard".

Once all the players have completed their hands, it is the dealer's turn. The dealer hand will not be completed if all players have either busted or received blackjacks. The dealer then reveals the hidden card and must hit until the cards total up to 17 points. At 17 points or higher the dealer must stay. (At most tables the dealer also hits on a "soft" 17, i.e. a hand containing an ace and one or more other cards totaling six.) You are betting that you have a better hand than the dealer. The better hand is the hand where the sum of the card values is closer to 21 without exceeding 21. The detailed outcome of the hand follows:

If the player is dealt an Ace and a ten-value card (called a "blackjack" or "natural"), and the dealer does not, the player wins and usually receives a bonus.
If the player exceeds a sum of 21 ("busts"); the player loses, even if the dealer also exceeds 21.
If the dealer exceeds 21 ("busts") and the player does not; the player wins.
If the player attains a final sum higher than the dealer and does not bust; the player wins.
If both dealer and player receive a blackjack or any other hands with the same sum called a "push", no one wins. 
*/

public class Dealer : MonoBehaviour
{
    public static Dealer instance;

    Deck deck = new Deck();

    [SerializeField] BettingSystem betting;


    [SerializeField] Settings settings;

    [SerializeField] bool dealerTurn = false;

    [SerializeField] List<Card> cards = new List<Card>();

    [SerializeField] Card lastCardDrawn;


    [SerializeField] int deckCount;

    [SerializeField] int playerCount;

    [SerializeField] int currentPlayerIndex = 0;


    [SerializeField] Hand dealerHand; // This will always be index 0 in the plate

    [SerializeField] List<Hand> playerHands = new List<Hand>();

    [SerializeField] GameObject playerButtons;

    [SerializeField] GameObject betWindow;

    

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

       private void Update()
    {
        if (dealerTurn)
        {
            if (dealerHand.handValue <= 17)
            {
                print("Dealer must hit");
                dealerHand.cards.Add(GetNextCard());
                dealerHand.CalulateHandValue();
                dealerHand.DestroyHandObjcet();
                UiManager.instance.RenderCards(dealerHand, true);
                UiManager.instance.ShowCards();
            }
            else
            {
                print("Greater than 17 don't have to do anything");
                foreach (Hand h in playerHands)
                {
                    GetWinner(h);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AnotherRound();
        }
    }

    public void HideButtons()
    {
        playerButtons.SetActive(false);
    }

    public Hand GetDealerHand()
    {
        return dealerHand;
    }



    /// <summary>
    ///
    /// </summary>
    public void CreateDecks()
    {
        print("Creating Deck");
        List<Card> tempDeck = new List<Card>();
        tempDeck = deck.CreatDeck();
        foreach (Card c in tempDeck)
        {
            cards.Add(c);
        }
    }

    /// <summary>
    /// Creates decks based on the deck count and Shuffel the deck when all cards are loaded
    /// </summary>
    public void CreateAndSuffelDecks()
    {
        for (int i = 0; i < deckCount; i++)
        {
            CreateDecks();
        }
        deck.ShuffeDeck(cards);
    }

    /// <summary>
    /// This class does something.
    /// </summary>
    public void Start()
    {
        CreateAndSuffelDecks();
        CreatePlayers();
    }



    /// <summary>
    /// This class does something.
    /// </summary>
    void CreatePlayers()
    {
        for (int i = 0; i < playerCount; i++)
        {
            Hand h = new Hand();
            playerHands.Add(h);
            UiManager.instance.UpdatePlayerAmount(h.money);
        }
    }

    /// <summary>
    /// This class does something.
    /// </summary>
    void StartRound()
    {
        dealerHand.cards.Add(GetNextCard());
        dealerHand.cards.Add(GetNextCard());
        dealerHand.CalulateHandValue();
        foreach (Hand h in playerHands)
        {
            h.cards.Add(GetNextCard());
            h.cards.Add(GetNextCard());
            h.CalulateHandValue();
            UiManager.instance.RenderCards(h);
            UiManager.instance.UpdatePlayerAmount(h.money);
        }
        UiManager.instance.RenderCards(dealerHand, true);
        UiManager.instance.HideSecondCard();

        playerButtons.SetActive(true);
    }

    public void AnotherRound()
    {
        dealerTurn = false;
        dealerHand.ResetHand();
        foreach (Hand h in playerHands)
        {
            h.ResetHand();
        }
        betWindow.SetActive(false);
        UiManager.instance.StatusText("");
        StartRound();
    }

    public void Bet()
    {

        var betAmount = UiManager.instance.GetBetAmount();
        var currentPlayer = playerHands[currentPlayerIndex];
        if (betAmount > settings.GetMin())
        {
            if (currentPlayer.CheckBet(betAmount))
            {
                betting.AddToPot(betAmount);
                playerHands[currentPlayerIndex].money -= betAmount;
                UiManager.instance.UpdateTotal();
                betWindow.SetActive(false);
                AnotherRound();
            }
        }
        else
        {
            print("Need to bet more the the min " + settings.GetMin());

            UiManager.instance.StatusText("Need to bet more the the min " + settings.GetMin());
        }

    }


    public void Stay()
    {
        print("Player Stay");
        if (currentPlayerIndex == playerHands.Count - 1)
        {
            print("All players have gone");
            dealerTurn = true;
        }
        else
        {
            print("Go to next player");
            currentPlayerIndex++;
        }
        playerButtons.SetActive(false);
    }

    public void Hit()
    {
        print("Player hit");
        var currentPlayer = playerHands[currentPlayerIndex];
        currentPlayer.cards.Add(GetNextCard());
        currentPlayer.CalulateHandValue();
        currentPlayer.DestroyHandObjcet();
        UiManager.instance.RenderCards(currentPlayer);
        if (currentPlayer.bust)
        {
            print("Bust player loses");
            UiManager.instance.StatusText("Bust you lose");
            betWindow.SetActive(true);
            playerButtons.SetActive(false);
            UiManager.instance.ShowCards();
        }
    }

    public void GetWinner(Hand player)
    {
        print("dealer" + dealerHand.handValue + " Player " + player.handValue);
        if (player.blackJack && !dealerHand.blackJack)
        {
            print("Player wins $" + betting.TriplePot());
            UiManager.instance.StatusText("BLACKJACK You won $" + betting.TriplePot());
        }
        else if (dealerHand.bust && !player.bust)
        {
            print("Player wins $" + betting.DoublePot());
            UiManager.instance.StatusText("You won $" + betting.DoublePot());
            player.money += betting.DoublePot();

        }
        else if(dealerHand.handValue == player.handValue)
        {
            print("Push");
        }
        else if (!dealerHand.bust && player.bust)
        {
            print("Dealer wins");
            UiManager.instance.StatusText("You Lose ");

        }
        else if (dealerHand.handValue < player.handValue)
        {
            print("Player wins $" + betting.DoublePot());
            player.money += betting.DoublePot();
            UiManager.instance.StatusText("You won $" + betting.DoublePot());
        }
        else if (dealerHand.handValue > player.handValue)
        {
            print("Dealer wins");
            UiManager.instance.StatusText("You Lose ");

        }
        betting.ClearPot();
        UiManager.instance.UpdateTotal();
        UiManager.instance.UpdatePlayerAmount(player.money);
        dealerTurn = false;
        betWindow.SetActive(true);
    }

    /// <summary>
    /// This class does something.
    /// </summary>
    /// /// <returns>
    /// The next card in the list of cards
    /// </returns>
    public Card GetNextCard()
    {
        if (cards.Count > 0)
        {
            lastCardDrawn = cards[0];
            cards.Remove(lastCardDrawn);
            return lastCardDrawn;
        }
        else
        {
            return null;
        }
    }
}




[System.Serializable]
public class Hand
{
    public List<Card> cards = new List<Card>();

    public List<GameObject> rendredCards = new List<GameObject>();

    public float money = 500;

    public int handValue;

    public bool bust;

    public bool blackJack;

    public void CheckHandResults()
    {
        if (handValue > 21)
        {
            bust = true;
        }
        else if (handValue == 21)
        {
            blackJack = true;
            Dealer.instance.HideButtons();
        }
    }

    public void CalulateHandValue()
    {
        handValue = 0;
        foreach (Card c in cards)
        {
            if (c.value <= 10 && c.value != 14)
            {
                handValue += c.value;
            }
            else if (c.value == 14)
            {
                if(handValue > 21)
                {
                    handValue += 11;
                }
                else
                {
                    handValue += 1;
                }
                
            }
            else
            {
                handValue += 10;
            }
        }
        CheckHandResults();
    }

    public void ResetHand()
    {
        cards.Clear();
        bust = false;
        blackJack = false;
        DestroyHandObjcet();
        handValue = 0;
    }

    public void DestroyHandObjcet()
    {
        foreach (GameObject g in rendredCards)
        {
            GameObject.Destroy(g);
        }
        rendredCards.Clear();
    }

    public bool CheckBet(float value)
    {

        var temp = money - value;

        if (money < 0)
        {
            Debug.Log("Invalid bet");
            return false;
        }
        else
        {
            Debug.Log("Vaild Bet");
            UiManager.instance.UpdatePlayerAmount(money);
            return true;
        }
    }
}

