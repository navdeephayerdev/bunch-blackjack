﻿using System.Collections.Generic;
using UnityEngine;

/*
Load Texture assets from resouce folder
Create Deck
Add Texture assets to card
*/

public class Deck
{
    // Start is called before the first frame update
    List<char> suits = new List<char> { 'C', 'S', 'H', 'D' };

    List<Texture2D> cardAsset = new List<Texture2D>();

    private void LoadTextures()
    {
        Object[] cards = Resources.LoadAll("Cards", typeof(Texture2D));
        foreach (object c in cards)
        {
            cardAsset.Add((Texture2D)c);
        }
    }
    public List<Card> CreatDeck()
    {
        int suitIndex = -1;
        List<Card> cards = new List<Card>();
        for (int i = 2; i < 6; i++)
        {
            suitIndex++;
            for (int j = 2; j < 15; j++)
            {
                Card c = new Card();
                c.suit = suits[suitIndex];
                c.value = j;
                cards.Add(c);
            }
        }
        LoadTextures();
        AddCardAsset(cards);
        return cards;
    }
    private List<Card> AddCardAsset(List<Card> newDeck)
    {
        string card;
        List<Card> deck = newDeck;
        foreach (Card c in deck)
        {
            card = c.value + "" + c.suit;
            for (int i = 0; i < newDeck.Count; i++)
            {
                if (card == cardAsset[i].name)
                {
                    c.cardAsset = cardAsset[i];
                    break;
                }
            }
        }
        return deck;
    }

    public List<Card> ShuffeDeck(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            Card temp = cards[i];
            int randomIndex = Random.Range(i, cards.Count);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
        return cards;
    }
}

[System.Serializable]
public class Card
{
    public int value;
    public char suit;
    public Texture2D cardAsset;
}