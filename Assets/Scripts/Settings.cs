﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    [SerializeField] float minBet = 10;
    [SerializeField] float maxBet = 100;
    [SerializeField] float startAmount = 500;

    public void SetMin(float value)
    {
        minBet = value;
    }

    public float GetMin()
    {
        return minBet;
    }

    public void SetMax(float value)
    {
        maxBet = value;
    }
    public float GetMax()
    {
        return maxBet;
    }

    public void SetStartingAmount(float value)
    {
        startAmount = value;
    }

    public float GetStartingAmount()
    {
        return startAmount;
    }

}
