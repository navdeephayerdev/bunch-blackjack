﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions {

    public static Vector3 GetMiddle (Vector3 pointA, Vector3 pointB) {
        Vector3 middlepoint = Vector3.zero;
        middlepoint = new Vector3((pointA.x + pointB.x)/2,(pointA.y + pointB.y)/2);
        return middlepoint;
    }
    public static Vector3 AddOffset(Vector3 obj,Vector3 offset){
        Vector3 addOffset = new Vector3(obj.x + offset.x,obj.y + offset.y, obj.z + offset.z);
        return addOffset;
        
    }
}