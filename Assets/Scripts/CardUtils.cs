﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CardUtils {

    public static GameObject Scale (GameObject card, float scaleFactor, bool bigger = false) {
        Vector3 localScale = card.transform.localScale;
        if (bigger) {
            card.transform.localScale = new Vector3 (localScale.x * scaleFactor, localScale.y * scaleFactor, localScale.z * scaleFactor);
        } else {
            card.transform.localScale = new Vector3 (localScale.x / scaleFactor, localScale.y / scaleFactor, localScale.z / scaleFactor);
        }
        return card;
    }

    public static GameObject CreateCard (Card newCard) {
        GameObject card = new GameObject ();
        CardObject cardObj = card.AddComponent<CardObject> ();
        card.name = newCard.value + "" + newCard.suit;
        SpriteRenderer cardSprite = card.AddComponent<SpriteRenderer> ();
        if (newCard.cardAsset == null) {
            Debug.Log ("Card asssets is null");
        } else {
            cardSprite.sprite = Sprite.Create (newCard.cardAsset, new Rect (0, 0, newCard.cardAsset.width, newCard.cardAsset.height), new Vector2 (0.5f, 0.5f));
        }
        cardObj.setCard (newCard);
        return card;
    }

    public static GameObject CreateCardBack () {
        GameObject cardBack = new GameObject ();
        SpriteRenderer cardSprite = cardBack.AddComponent<SpriteRenderer> ();
        Texture2D back = Resources.Load ("Cards/Backs/Back") as Texture2D;
        cardSprite.sprite = Sprite.Create (back, new Rect (0, 0, back.width, back.height), new Vector2 (0.5f, 0.5f));
        return cardBack;
    }

    public static Sprite GetBackSprite()
    {
        Texture2D back = Resources.Load("Cards/Backs/Back") as Texture2D;
        Sprite cardBack = Sprite.Create(back, new Rect(0, 0, back.width, back.height), new Vector2(0.5f, 0.5f));
        return cardBack;
    }

}