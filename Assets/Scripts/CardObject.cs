﻿using UnityEngine;

public class CardObject : MonoBehaviour {

    [SerializeField] Card card;


    public void setCard (Card card) {
        this.card = card;
    }


    public Card GetCard () {
        return card;
    }

}