﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    [SerializeField] Transform dealCardPos;

    [SerializeField] Transform playerCardPos;

    [SerializeField] Text betAmount;

    [SerializeField] Slider betSlider;

    [SerializeField] BettingSystem betting;

    [SerializeField] Settings settings;

    [SerializeField] Text potText;

    [SerializeField] Text playerMoney;

    [SerializeField] Text playerNotify;

    [SerializeField] InputField min;

    [SerializeField] InputField max;

    [SerializeField] InputField statingAmount;

    [SerializeField] Sprite cardBack;

    [SerializeField] Sprite ogSprite;


    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
        betAmount.text = "Bet Amount : $ " + betSlider.value;
    }


    public void HideSecondCard()
    {
        var temp = Dealer.instance.GetDealerHand();
        ogSprite = temp.rendredCards[1].GetComponent<SpriteRenderer>().sprite;
        temp.rendredCards[1].GetComponent<SpriteRenderer>().sprite = cardBack;

    }

    public void ShowCards()
    {
        var temp = Dealer.instance.GetDealerHand();
        temp.rendredCards[1].GetComponent<SpriteRenderer>().sprite = ogSprite;
    }



    public void RenderCards(Hand hand, bool dealer = false)
    {
        Vector3 lastCardPos = Vector3.zero;
        GameObject temp;
        bool first = true;
        foreach (Card c in hand.cards)
        {
            temp = CardUtils.CreateCard(c);
            if (first)
            {
                if (dealer)
                {
                    temp.transform.position = dealCardPos.position;
                    dealCardPos.GetChild(0).GetComponent<Text>().text = hand.handValue.ToString();
                }
                else
                {
                    temp.transform.position = playerCardPos.position;
                    playerCardPos.GetChild(0).GetComponent<Text>().text = "Card Count :" + hand.handValue.ToString();
                }
                first = false;
            }
            else
            {

                temp.transform.position = new Vector3(lastCardPos.x + 3, lastCardPos.y, lastCardPos.z + 0.1f);
            }
            lastCardPos = temp.transform.position;
            CardUtils.Scale(temp, 0.6f);
            hand.rendredCards.Add(temp);
        }
        first = true;
    }

    public void UpdatePlayerAmount(float value)
    {
        playerMoney.text = "Total : $" + value;
    }

    public void UpdateTotal()
    {
        potText.text = "Total Pot : $" + betting.GetPot();
    }

    public float GetBetAmount()
    {
        return betSlider.value;
    }
    public void UpdateBetAmount()
    {
        betAmount.text = "Bet Amount : $" + betSlider.value;
    }

    public void StatusText(string text, bool bad = false)
    {
        playerNotify.text = text;
        if (bad)
        {
            playerNotify.color = Color.red;
        }
        else
        {
            playerNotify.color = Color.white;
        }
    }

    public void UpdateMin()
    {
        settings.SetMin(float.Parse(min.text));
        betSlider.minValue = settings.GetMin();
    }

    public void UpdateMax()
    {
        settings.SetMax(float.Parse(max.text));
        betSlider.maxValue = settings.GetMax();
    }

    public void UpdateStartingAmount()
    {
        settings.SetMin(float.Parse(statingAmount.text));
    }

}
