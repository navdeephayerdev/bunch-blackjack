﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BettingSystem : MonoBehaviour
{

    
    float pot;

    public void AddToPot(float value)
    {
        pot += value;
    }

    public float GetPot()
    {
        return pot;
    }

    public float DoublePot()
    {
        return pot * 2;
    }

    public float TriplePot()
    {
        return pot * 3;
    }

    public void ClearPot()
    {
        pot = 0;
    }
}
